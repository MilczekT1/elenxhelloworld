package pl.konradboniecki.ElenxHelloWorld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElenxHelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElenxHelloWorldApplication.class, args);
	}
}
